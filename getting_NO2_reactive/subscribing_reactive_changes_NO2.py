
'''
This code is a example for make a connection from a Reactive Box Mobility Madrid
The Reactive Box is a Meteor Server which includes many layers of data.
Current example shows the way of make subscription at NO2 sensors of Madrid City Council

For use this program must be include:
MeteorClient https://github.com/hharnisc/python-meteor
EventEmitter https://github.com/jfhbrook/pyee
Websockets (ws4py on https://github.com/Lawouach/WebSocket-for-Python)
ddpClient https://pypi.python.org/pypi/python-ddp/0.1.0
'''
import time
import datetime
import pika
import json
import sys
from MeteorClient import MeteorClient



def subscribed(subscription):
    print('* SUBSCRIBED {}'.format(subscription))


def unsubscribed(subscription):
    print('* UNSUBSCRIBED {}'.format(subscription))


def added(collection, id, fields):
    # print('* ADDED {} {}'.format(collection, id))

    if collection == "users":
        # print collection
        client.subscribe('SENNO2MAD.eventpos.all')
        # Optional, you may include filters using the "custom" property. The filters for using is equal to API REST examples including in this folder.
        # Example to observe geofence
        # client.subscribe("SENNO2MAD.eventpos.custom",[{"geometry": {"$near": {"$geometry": {"type": "Point" ,"coordinates": [-3.692045, 40.408812 ]},"$maxDistance": 300,"$minDistance": 0}}}])
        # Example to observe one station
        # client.subscribe("SENNO2MAD.eventpos.custom",[{"idStation": "28079099" }])
    elif collection == "SENNO2MAD.eventpos":
        try:
            print("subscribed..." + str(collection))
            print ("******" + str(fields))

        except:

            msgLog = "Error in data %s" % (sys.exc_info()[1])
            print(msgLog)



def changed(collection, id, fields, cleared):
    try:
        print("changed:{}".format(id) + " fields: {}".format(fields))

    except:

        msgLog = "Error in data changed %s" % (sys.exc_info()[1])
        print(msgLog)


def connected():
    print('* CONNECTED')

    client.login("your mail", "your password")


def subscription_callback(error):
    if error:
        print(error)


try:

    client = MeteorClient('ws://rbmobility.emtmadrid.es:4444/websocket', auto_reconnect=True, auto_reconnect_timeout=5,
                          debug=False)

    client.on('subscribed', subscribed)
    client.on('unsubscribed', unsubscribed)
    client.on('added', added)
    client.on('connected', connected)
    client.on('changed', changed)

    client.connect()

    # ctrl + c to kill the script
    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            break


except Exception as err:
    print (err)